"""Pipeline trigger for baselines.

1. Get current top commit of the requested repo/branch.
2. Check if it was already tested.
3. Profit $$$.
"""
import copy
import json

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib.logger import get_logger
from cki_lib.misc import utc_now_iso

from . import utils

LOGGER = get_logger(__name__)


def load_triggers(gitlab_instance, baselines_config, _):
    """Get ready all potential triggers."""
    triggers = []
    for key, value in baselines_config.items():
        for branch in value['.branches']:
            trigger = copy.deepcopy(value)
            project = cki_pipeline.pipeline_project(gitlab_instance, trigger)

            trigger['commit_hash'] = utils.get_commit_hash(
                trigger['git_url'], f'refs/heads/{branch}'
            )
            if trigger['commit_hash'] is None:
                continue

            trigger['discovery_time'] = utc_now_iso()
            trigger['branch'] = branch
            if not trigger.get('name'):
                trigger['name'] = key
            if report_rules := trigger.get('.report_rules'):
                trigger['report_rules'] = json.dumps(report_rules)

            # Check the URL and branch separately - it's possible to have the same
            # branch name in different repos people use, e.g. version based naming
            if 'watch_url' not in trigger:
                trigger['watch_url'] = trigger['git_url']
            if 'watch_branch' not in trigger:
                trigger['watch_branch'] = trigger['branch']
            trigger['watched_repo_commit_hash'] = utils.get_commit_hash(
                trigger['watch_url'], f'refs/heads/{trigger["watch_branch"]}'
            )

            do_not_trigger = utils.was_tested(
                project,
                trigger['cki_pipeline_branch'],
                trigger['watched_repo_commit_hash'],
                {
                    'watch_url': trigger['watch_url'],
                    'watch_branch': trigger['watch_branch'],
                    'package_name': trigger.get('package_name'),
                },
            )

            if do_not_trigger:
                LOGGER.info('Pipeline for %s@%s already triggered. Watched %s@%s',
                            trigger['branch'],
                            trigger['commit_hash'],
                            trigger['watch_branch'],
                            trigger['watch_url'])
            else:
                title = [
                    'Baseline:',
                    trigger["name"],
                    f'{trigger["branch"]}:{trigger["commit_hash"][0:12]}'
                ]
                if (package_name := trigger.get('package_name', 'kernel')) != 'kernel':
                    title += [f'({package_name})']
                trigger['title'] = ' '.join(title)
                triggers.append(config_tree.clean_config(trigger))
    return triggers
